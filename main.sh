#!/bin/bash
source commands.sh
source profile.sh

cmd() {
  cmd=$1
  argv=$*

  case "${cmd}" in
    help)
      help;;
    ls)
      list_files;;
    rm)
      remove_file $argv;;
    rmd|rmdir)
      remove_dir $argv;;
    about)
      about;;
    version|--v|vers)
      version;;
    age)
      check_age;;
    quit|exit)
      quit;;
    profil)
      show_profile;;
    passw)
      change_password;;
    cd)
      change_directory $argv;;
    pwd)
      print_working_dir;;
    hour)
      print_hour;;
    httpget)
      http_get $argv;;
    smtp)
      send_mail $argv;;
    open)
      open_file $argv;;
    *)
      echo "Unknown command: $cmd"
      ;;
  esac
}

main() {
  echo "Welcome! Please login to access the custom shell prompt."
  if ! check_credentials; then
    exit 1  
  fi

  lineCount=1
  while [ 1 ]; do
    date=$(date +%H:%M)
    echo -ne "${date} - [\033[31m${lineCount}\033[m] - \033[33mXzen\033[m ~ ☠️ ~ "
    read string
    cmd $string
    lineCount=$(($lineCount+1))
  done
}

main
