#!/bin/bash
source quit.sh
source profile.sh

help() {
  echo "Commands: help, ls, rm, rmd, about, version, age, quit, profil, passw, cd, pwd, hour, httpget, smtp, open"
}

list_files() {
  ls -la
}

remove_file() {
  rm -i "$2"
}

remove_dir() {
  rm -ri "$2"
}

about() {
  echo "This is a custom shell prompt script for learning purposes."
}

version() {
  echo "Version 1.0"
}

check_age() {
  read -p "Enter your age: " age
  if [[ $age -ge 18 ]]; then
    echo "You are an adult."
  else
    echo "You are a minor."
  fi
}

show_profile() {
  display_profile
}

change_password() {
  read -sp "Enter new password: " new_pass
  echo
  read -sp "Confirm new password: " confirm_pass
  echo
  if [ "$new_pass" == "$confirm_pass" ]; then
    echo "Password changed!"
  else
    echo "Passwords do not match!"
  fi
}

change_directory() {
  cd "$2"
}

print_working_dir() {
  pwd
}

print_hour() {
  echo $(date +"%H:%M")
}

http_get() {
  read -p "Enter URL: " url
  read -p "Enter file name to save as: " filename
  curl -o "$filename" "$url"
}

send_mail() {
  read -p "Enter email address: " email
  read -p "Enter subject: " subject
  read -p "Enter message: " message
  echo "$message" | mail -s "$subject" $email
}

open_file() {
  vim "$2"
}
