# Prompt Personnalisé

## Description
Ce projet consiste en un prompt de shell personnalisé qui offre des fonctionnalités étendues pour une utilisation interactive. Il inclut des commandes pour la gestion des fichiers, la navigation dans le système, et des utilitaires personnalisés pour l'interaction avec le système d'exploitation.

## Fonctionnalités
- **Authentification** : Accès sécurisé au prompt par un système de login et de mot de passe.
- **Navigation** : Commandes pour changer de répertoire et afficher le chemin actuel.
- **Gestion des fichiers** : Possibilité de lister, supprimer des fichiers et des dossiers.
- **Information système** : Affiche l'heure et des informations sur la version du prompt.
- **Profil utilisateur** : Affiche et permet de mettre à jour les informations du profil utilisateur.
- **Téléchargement** : Télécharge le code source HTML d'une page web spécifiée.
- **Email** : Envoie un email avec un sujet et un corps spécifiés.
- **Edition** : Ouvre un fichier dans l'éditeur VIM, même si le fichier n'existe pas.

## Installation

1. **Clonez le dépôt** :
   ```bash
   git clone https://gitlab.com/juliencln1s1/prompt-shell-personnalise.git
2. **Utilisation** :
    ```bash
    ./main.sh
    ```
    Puis connectez-vous  
    Login : user  
    Password : 123

3. **Commandes disponibles** :  

    help : Affiche une liste de toutes les commandes disponibles.  
    ls : Liste les fichiers et dossiers dans le répertoire courant, y compris les fichiers cachés.  
    rm <fichier> : Supprime un fichier spécifié.  
    rmd <dossier> ou rmdir <dossier> : Supprime un dossier spécifié.  
    about : Affiche une description du programme.  
    version ou --v ou vers : Affiche la version du prompt.  
    age : Demande votre âge et indique si vous êtes majeur ou mineur.  
    quit : Quitte le prompt.  
    profil : Affiche des informations sur le profil utilisateur.  
    passw : Permet de changer le mot de passe avec confirmation.   
    cd <dossier> : Change le répertoire courant.  
    pwd : Affiche le chemin du répertoire courant.  
    hour : Affiche l'heure actuelle.  
    httpget <url> <nom_fichier> : Télécharge le code source d'une page web dans un fichier spécifié.  
    smtp <email> <sujet> <message> : Envoie un email.  
    open <fichier> : Ouvre un fichier dans VIM.  