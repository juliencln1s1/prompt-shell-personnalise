#!/bin/bash

# Chemin du fichier de données
CREDENTIALS_FILE="credentials.txt"

# Lire les données d'authentification du fichier
read_credentials() {
  USERNAME=$(sed -n '1p' $CREDENTIALS_FILE | tr -d '\r\n ')
  PASSWORD=$(sed -n '2p' $CREDENTIALS_FILE | tr -d '\r\n ')
  echo "Debug: Username is $USERNAME, Password is $PASSWORD"
}

# Mettre à jour le mot de passe dans le fichier
update_credentials() {
  echo "$USERNAME" > $CREDENTIALS_FILE
  echo "$PASSWORD" >> $CREDENTIALS_FILE
}

# Informations de profil
FIRST_NAME="Julien"
LAST_NAME="Coulon"
AGE="23"
EMAIL="juliencln1s1@gmail.com"

# Fonction pour afficher le profil
display_profile() {
  echo "First Name: $FIRST_NAME"
  echo "Last Name: $LAST_NAME"
  echo "Age: $AGE"
  echo "Email: $EMAIL"
}

# Fonction pour vérifier les identifiants de connexion
check_credentials() {
  read_credentials
  read -p "Login: " input_login
  read -sp "Password: " input_password
  echo

  if [[ $input_login == $USERNAME && $input_password == $PASSWORD ]]; then
    echo "Access Granted."
    return 0
  else
    echo "Access Denied. Incorrect login or password."
    return 1
  fi
}

change_password() {
  echo "Changing password..."
  read -sp "Enter current password: " current_pass
  echo

  if [[ $current_pass != $PASSWORD ]]; then
    echo "Current password is incorrect!"
    return 1
  fi

  read -sp "Enter new password: " new_pass
  echo
  read -sp "Confirm new password: " confirm_pass
  echo

  if [[ "$new_pass" == "$confirm_pass" ]]; then
    PASSWORD=$new_pass
    update_credentials
    echo "Password changed successfully!"
  else
    echo "Passwords do not match!"
  fi
}